//
//  DeviceDetailController.swift
//  SwiftBLEBeacon
//
//  Created by Danilo Altheman on 18/06/14.
//  Copyright (c) 2014 Quaddro. All rights reserved.
//
import Foundation

import UIKit
import CoreBluetooth
class DeviceDetailController: UIViewController, CBPeripheralDelegate {
    @IBOutlet var temperatureLabel: UILabel
    @IBOutlet var relativeHumidityLabel: UILabel
    @IBOutlet var logoImageView: UIImageView
    
    var currentPeripheral: CBPeripheral!
    
    override func viewDidLoad()  {
        super.viewDidLoad()
        var blur:UIBlurEffect = UIBlurEffect(style: UIBlurEffectStyle.Dark)
        var effectView:UIVisualEffectView = UIVisualEffectView (effect: blur)
        effectView.frame = self.view.frame
        self.logoImageView.addSubview(effectView)
        
        self.navigationItem.title = currentPeripheral.name
        currentPeripheral.delegate = self
        self.temperatureLabel.font = UIFont(name: "Helvetica Neue Thin", size: 64.0)
        
        // 1] Search for any service
        currentPeripheral.discoverServices(nil)
    }
    
    override func viewWillAppear(animated: Bool)  {
        super.viewWillAppear(animated)
        self.currentPeripheral = nil
    }
    // MARK: -- CBPeripheralDelegate
    
    // Discovered Services
    func peripheral(peripheral: CBPeripheral!, didDiscoverServices error: NSError!) {
        for service: AnyObject in peripheral.services {
            // 2] Discover Characteristic for discovered service.
            peripheral.discoverCharacteristics(nil, forService: service as CBService)
        }
    }
    
    // Discovered Characteristic
    func peripheral(peripheral: CBPeripheral!, didDiscoverCharacteristicsForService service: CBService!, error: NSError!) {
        for characteristic: AnyObject in service.characteristics {
            var cbUUID: CBUUID = characteristic.UUID
//            println("Service: \(service) -> Charac: \(cbUUID)")
            // 3] Check if Temperature sensor is present.
            
            /****************** Temperature ******************/
            if toString(cbUUID) == IR_TEMP_DATA {
                println("IR Temperature sensor found")
                // 4] if present then read value and notify
                peripheral.readValueForCharacteristic(characteristic as CBCharacteristic)
                peripheral.setNotifyValue(true, forCharacteristic: characteristic as CBCharacteristic)
            }
            if toString(cbUUID) == IR_TEMP_CONF {
                println("Writing to IR Temperature sensor")
                var byte: Byte[] = [0x01]
                var data = NSData(bytes: byte, length: byte.count)
                peripheral.writeValue(data, forCharacteristic: characteristic as CBCharacteristic, type: CBCharacteristicWriteType.WithResponse)
            }
            /*************************************************/
            
            
            if toString(cbUUID) == RELATIVE_HUMIDITY_DATA {
                println("Relative humidity sensor found")
                peripheral.readValueForCharacteristic(characteristic as CBCharacteristic)
                peripheral.setNotifyValue(true, forCharacteristic: characteristic as CBCharacteristic)
                // Subscribe for data notifications.
            }
            if toString(cbUUID) == RELATIVE_HUMIDITY_CONFIG {
                println("Writing to Relative humidity sensor")
                var byte: Byte[] = [0x01]
                var data = NSData(bytes: byte, length: byte.count)
                peripheral.writeValue(data, forCharacteristic: characteristic as CBCharacteristic, type: CBCharacteristicWriteType.WithResponse)
            }
        }
    }
    
    //http://processors.wiki.ti.com/index.php/SensorTag_User_Guide
    func peripheral(peripheral: CBPeripheral!, didUpdateValueForCharacteristic characteristic: CBCharacteristic!, error: NSError!) {
        if !error {
            if toString(characteristic.UUID) == toString(RELATIVE_HUMIDITY_DATA) {
                self.relativeHumidityLabel.text = String(format: "%.f%%", self.calculateRelativeHumidity(characteristic.value))
                self.temperatureLabel.text = String(format: "%.f°c", self.calculateAirTemperature(characteristic.value))
                println(self.calculateAirTemperature(characteristic.value))
            }
            
            if toString(characteristic.UUID) == toString(IR_TEMP_DATA) {
                
            }
        }
        else {
            println(error)
        }
    }
    
    
    // Calculate Relative Humidity
    func calculateRelativeHumidity(data: NSData) -> Float {
        let ptr = UnsafePointer<UInt8>(data.bytes)
        let rawData = UnsafeArray<UInt8>(start: ptr, length: data.length)
//        println(rawData[0], rawData[1], rawData[2], rawData[3])
        var humidity: UInt16 = UInt16((rawData[2] & 0xff)) | UInt16((UInt16(rawData[3]) << 8) & 0xff00)
        var relativeHumidity = -6.0 + 125.0 * Float(Float(humidity) / Float(65535))
        return relativeHumidity
    }
    
    func calculateAirTemperature(data: NSData) -> Float{
        let ptr = UnsafePointer<UInt8>(data.bytes)
        let rawData = UnsafeArray<UInt8>(start: ptr, length: data.length)
        var temperature: UInt16 = UInt16((rawData[0] & 0xff)) | UInt16((UInt16(rawData[1]) << 8) & 0xff00)
        // Convert raw temperature to Celsius Degrees
        var airTemperature = -46.65 + 175.72 / 65536.0 * Float(temperature)
        return Float(airTemperature)
    }
    
    
    func peripheral(peripheral: CBPeripheral!, didUpdateNotificationStateForCharacteristic characteristic: CBCharacteristic!, error: NSError!) {
        println("didUpdateNotificationStateForCharacteristic")
    }
    
    
//    func peripheralDidUpdateRSSI(peripheral: CBPeripheral!, error: NSError!) {
//        println("\(peripheral.RSSI)")
//        self.rssiLabel.text = toString(peripheral.RSSI)
//    }
}
