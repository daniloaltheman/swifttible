//
//  ViewController.swift
//  SwiftBLEBeacon
//
//  Created by Danilo Altheman on 18/06/14.
//  Copyright (c) 2014 Quaddro. All rights reserved.
//

import UIKit
import CoreBluetooth

class DevicesViewController: UITableViewController, CBCentralManagerDelegate, CBPeripheralDelegate {
    var centralManager:CBCentralManager!
    var currentPeripheral: CBPeripheral!
    var bleDevices = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.centralManager = CBCentralManager(delegate: self, queue: nil, options: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: -- TableViewDataSource / TableViewDelegate
    override func numberOfSectionsInTableView(tableView: UITableView!) -> Int  {
        return 1
    }
    
    override func tableView(tableView: UITableView!, numberOfRowsInSection section: Int) -> Int  {
        return self.bleDevices.count
    }
    
    override func tableView(tableView: UITableView!, cellForRowAtIndexPath indexPath: NSIndexPath!) -> UITableViewCell!  {
        var cell = tableView.dequeueReusableCellWithIdentifier("CellIdentifier", forIndexPath: indexPath) as UITableViewCell
        var peripheral: CBPeripheral = self.bleDevices.objectAtIndex(indexPath.row) as CBPeripheral

        cell.textLabel.text = self.bleDevices[indexPath.row].name
        cell.detailTextLabel.text = (peripheral.state == .Connected) ? "Connected" : "Disconnected"
        switch(peripheral.state) {
            case .Connecting:
                cell.detailTextLabel.text = "Connecting"
            case .Connected:
                cell.detailTextLabel.text = "Connected"
            default:
                cell.detailTextLabel.text = "Disconnected"
        }
        return cell
    }
    
    override func tableView(tableView: UITableView!, didSelectRowAtIndexPath indexPath: NSIndexPath!)  {
        var peripheral: CBPeripheral = self.bleDevices.objectAtIndex(indexPath.row) as CBPeripheral
        if peripheral.state == .Disconnected {
            self.centralManager.connectPeripheral(self.bleDevices.objectAtIndex(indexPath.row) as CBPeripheral, options: nil)
        }
        else if peripheral.state == .Connected {
            self.performSegueWithIdentifier("DeviceDetailController", sender: indexPath)
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!)  {
        var detailsController: DeviceDetailController = segue.destinationViewController as DeviceDetailController
        detailsController.currentPeripheral = self.bleDevices.objectAtIndex(sender.row) as CBPeripheral
    }
    
    // MARK: -- CBCentralManagerDelegate
    func centralManagerDidUpdateState(central: CBCentralManager!) {
        println("centralManagerDidUpdateState")
        switch(centralManager!.state) {
        case CBCentralManagerState.PoweredOn:
            println("bluetooth state: Powered On")
            self.centralManager?.scanForPeripheralsWithServices(nil, options: nil)
            
        case .PoweredOff:
            println("Bluetooth state: Powered Off")
            self.bleDevices.removeAllObjects()
            self.tableView.reloadData()
            
        case .Unauthorized:
            println("Bluetooth state: Unauthorized")
            
        case .Unsupported:
            println("Bluetooth state: Not supported device")
            
        case .Unknown:
            println("Bluetooth state: Unknown")
            
        case .Resetting:
            println("Bluetooth state: Resetting")
            
        default:
            println("Bluetooth state UNDEFINED")
        }
    }
    
    func centralManager(central: CBCentralManager!, didDiscoverPeripheral peripheral: CBPeripheral!, advertisementData: NSDictionary!, RSSI: NSNumber!) {
//        377C1B7A-2DFF-A110-85A5-487F5F242184
        // Look only for Sensor Tag!
        if peripheral.name {
            if peripheral.name == "TI BLE Sensor Tag" {
                println("Waiting for connection with SensorTag")
                peripheral.delegate = self
                self.bleDevices.addObject(peripheral)
                self.tableView.reloadData()
            }
        }
    }
    
    func centralManager(central: CBCentralManager!, didConnectPeripheral peripheral: CBPeripheral!)  {
        tableView.reloadData()
    }
    
    func centralManager(central: CBCentralManager!, didDisconnectPeripheral peripheral: CBPeripheral!, error: NSError!)  {
        println("didDisconnectPeripheral")
        self.bleDevices.removeObject(peripheral)
        self.tableView.reloadData()
        self.centralManager.scanForPeripheralsWithServices(nil, options: nil)
    }
    
    func centralManager(central: CBCentralManager!, didFailToConnectPeripheral peripheral: CBPeripheral!, error: NSError!)  {
        println("didFailToConnectPeripheral")
    }
}

