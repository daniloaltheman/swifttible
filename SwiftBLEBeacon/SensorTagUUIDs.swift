//
//  SensorTagUUID.swift
//  SensorTag
//
//  Created by Danilo Altheman on 18/06/14.
//  Copyright (c) 2014 Danilo C. Altheman. All rights reserved.
//

import Foundation
// Get all UUID sensors from TI.com
let IR_TEMP_SERVICE  = "F000AA00-0451-4000-B000-000000000000"
let IR_TEMP_DATA  = "F000AA01-0451-4000-B000-000000000000"
let IR_TEMP_CONF = "F000AA02-0451-4000-B000-000000000000"

let RELATIVE_HUMIDITY_SERVICE = "F000AA20-0451-4000-B000-000000000000"
let RELATIVE_HUMIDITY_DATA = "F000AA21-0451-4000-B000-000000000000"
let RELATIVE_HUMIDITY_CONFIG = "F000AA22-0451-4000-B000-000000000000"


/*
// First we set ambient temperature
[d setValue:@"1" forKey:@"Ambient temperature active"];
// Then we set IR temperature
[d setValue:@"1" forKey:@"IR temperature active"];
// Append the UUID to make it easy for app
[d setValue:@"f000aa00-0451-4000 b000-000000000000"  forKey:@"IR temperature service UUID"];
[d setValue:@"f000aa01-0451-4000 b000-000000000000" forKey:@"IR temperature data UUID"];
[d setValue:@"f000aa02-0451-4000 b000-000000000000"  forKey:@"IR temperature config UUID"];

// Then we setup the accelerometer
[d setValue:@"1" forKey:@"Accelerometer active"];
[d setValue:@"500" forKey:@"Accelerometer period"];
[d setValue:@"f000aa10-0451-4000 b000-000000000000"  forKey:@"Accelerometer service UUID"];
[d setValue:@"f000aa11-0451-4000 b000-000000000000"  forKey:@"Accelerometer data UUID"];
[d setValue:@"f000aa12-0451-4000 b000-000000000000"  forKey:@"Accelerometer config UUID"];
[d setValue:@"f000aa13-0451-4000 b000-000000000000"  forKey:@"Accelerometer period UUID"];

//Then we setup the rH sensor
[d setValue:@"1" forKey:@"Humidity active"];
[d setValue:@"f000aa20-0451-4000 b000-000000000000"   forKey:@"Humidity service UUID"];
[d setValue:@"f000aa21-0451-4000 b000-000000000000" forKey:@"Humidity data UUID"];
[d setValue:@"f000aa22-0451-4000 b000-000000000000" forKey:@"Humidity config UUID"];

//Then we setup the magnetometer
[d setValue:@"1" forKey:@"Magnetometer active"];
[d setValue:@"500" forKey:@"Magnetometer period"];
[d setValue:@"f000aa30-0451-4000 b000-000000000000" forKey:@"Magnetometer service UUID"];
[d setValue:@"f000aa31-0451-4000 b000-000000000000" forKey:@"Magnetometer data UUID"];
[d setValue:@"f000aa32-0451-4000 b000-000000000000" forKey:@"Magnetometer config UUID"];
[d setValue:@"f000aa33-0451-4000 b000-000000000000" forKey:@"Magnetometer period UUID"];

//Then we setup the barometric sensor
[d setValue:@"1" forKey:@"Barometer active"];
[d setValue:@"f000aa40-0451-4000 b000-000000000000" forKey:@"Barometer service UUID"];
[d setValue:@"f000aa41-0451-4000 b000-000000000000" forKey:@"Barometer data UUID"];
[d setValue:@"f000aa42-0451-4000 b000-000000000000" forKey:@"Barometer config UUID"];
[d setValue:@"f000aa43-0451-4000 b000-000000000000" forKey:@"Barometer calibration UUID"];

[d setValue:@"1" forKey:@"Gyroscope active"];
[d setValue:@"f000aa50-0451-4000 b000-000000000000" forKey:@"Gyroscope service UUID"];
[d setValue:@"f000aa51-0451-4000 b000-000000000000" forKey:@"Gyroscope data UUID"];
[d setValue:@"f000aa52-0451-4000 b000-000000000000" forKey:@"Gyroscope config UUID"];

*/